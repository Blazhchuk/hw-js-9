"use strict"

/*

1.  DOM - це інтерфейс в вигляді дерева, де кожен елемент документа є вузлом цього вузла. DOM дозволяє динамічно змінювати контент, структуру та стиль документа.

2.  За допомогою innerHTML - можна додавати або змінювати теги в html коді. З innerText - може додавати або змінювати текст всередині елемента.

3.  Є такі способи зверненя до елементів:
    1.getElementById - цей метод шукає елемент за айдішником.
    2.getElementByClassName - повертає живу колекцію елементів за класом.
    3.getElementByTagName - повертає живу колекцію елементів тегом.
    4.querySelector - повертає перший елемент, який відповідає css селектору (class(.), id(#)).
    5.querySelectorAll - повертає не живу колекцію елементів, які відповідають css селектору.

    Кращого способу немає, всі способи однаково корисні при певних ситуаціях, при пошуку за классом, айдішником або ж групи тегів

4.  NodeList - це або статична або жива колекція вузлів, а HTMLCollection - завжди жива колекція елементів

*/


// ex 1

let featureQuery = document.querySelectorAll('.feature');

let fetureGetElem = document.getElementsByClassName('feature');

// console.log(featureQuery);
// console.log(fetureGetElem);

for (let i = 0; i < featureQuery.length; i++) {
    featureQuery[i].style.textAlign = 'center';
}


// ex 2

let h2 = document.getElementsByClassName('feature-title');

for (let i = 0; i < h2.length; i++) {
    h2[i].textContent = 'Awesome feature';
    h2[i].innerText += '!';
}
